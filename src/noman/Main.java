package noman;
import noman.character.Char;
import noman.weapons.*;
import java.util.*;
/**
 * Created by Normie on 8-5-17.
 */

public class Main {

    // The beginning of the application.
    // Here our application will start and create objects.
    public static void main(String[] args) {
        Random rand = new Random();
        Scanner input = new Scanner(System.in);

        // initialize the weapons for choosing.
        int damage = 0;
        Weapon staff = new Staff("Staff of glory");
        Weapon axe = new Axe("Woodcutter's Axe of death");
        Weapon dagger = new Dagger("Bronze OG Dagger");
        Weapon greatsword = new Greatsword("Malachite Great sword");
        Weapon longbow = new Longbow("Longbow of Agamar");
        Weapon longbow_2 = new Longbow("Longbow of Luuk");
        Weapon shortbow = new Shortbow("Shortbow of Ogomor");
        Weapon shortsword = new Shortsword("Short sword of light");
        String weapon_text = "Choose your weapon (type in the number corresponding to the weapon):" +
                "\n [1] " + staff.getName()      + "\t [can deal " + staff.getDamage()      + " damage]" +
                "\n [2] " + axe.getName()        + "\t [can deal " + axe.getDamage()        + " damage]" +
                "\n [3] " + dagger.getName()     + "\t [can deal " + dagger.getDamage()     + " damage]" +
                "\n [4] " + greatsword.getName() + "\t [can deal " + greatsword.getDamage() + " damage]" +
                "\n [5] " + longbow.getName()    + "\t [can deal " + longbow.getDamage()    + " damage]" +
                "\n [6] " + shortbow.getName()   + "\t [can deal " + shortbow.getDamage()   + " damage]" +
                "\n [7] " + shortsword.getName() + "\t [can deal " + shortsword.getDamage() + " damage]" +
                "\n [0] to quit \n > ";

        Char[] enemies = {new Char("Skelly the skeleton", 1 ,100), new Char("Jasmine the Coder", 99, 900), new Char("NORMAN", 1, 100), new Char("Luukie", 5, 450)};
        int enemyDamage = 10;
        int numHealthPots = 3;
        int healthPotAmount = 30;
        int healthPotDropRate = 50; // percentage

        int numAttackPots = 3;
        int attackPotAmount = 40;
        int attackPotDropRate = 20; // percentage

        boolean running = true;
        // Create a new character
        System.out.print("Your name please: ");
        String name = "";
        name += input.nextLine();

        Char p1 = new Char(name, 3, 300);
        p1.info();

        System.out.print(weapon_text);
        String weapon_choose = input.nextLine();

        switch(weapon_choose) {
            case "1":
                damage = staff.getDamage();
                p1.equip(staff);
                break;

            case "2":
                damage = axe.getDamage();
                p1.equip(axe);
                break;

            case "3":
                damage = dagger.getDamage();
                p1.equip(dagger);
                break;

            case "4":
                damage = greatsword.getDamage();
                p1.equip(greatsword);
                break;

            case "5":
                damage = longbow.getDamage();
                p1.equip(longbow);
                break;

            case "6":
                damage = shortbow.getDamage();
                p1.equip(shortbow);
                break;

            case "7":
                damage = shortsword.getDamage();
                p1.equip(shortsword);
                break;

            case "0":
                System.out.print("you have exited the game!");
                System.exit(0);
                break;
            default:
                System.out.println("Invalid number!");
                System.exit(0);
                break;
        }

        System.out.println("\t=============================");
        System.out.println("\tX          TEXTWAR          X");
        System.out.println("\t=============================");
        ROUND:
        while(running) {
            Char enemy = enemies[rand.nextInt(enemies.length)];
            int health = p1.getHealth();
            int enemyHealth = enemy.getHealth();
            enemy.introduce();

            while(enemy.getHealth() > 0) {
                System.out.println("\n\t your health: " + health);
                System.out.println("\t enemy's health: " + enemyHealth);
                System.out.println("\n\t What would you like to do?");
                System.out.println("\t [1] Attack");
                System.out.println("\t [2] Use a health potion");
                System.out.println("\t [3] Use an attack potion");
                System.out.print("\t [4] Run! \n\t > ");

                String choice = input.nextLine();

                if (choice.equals("1")) {
                    int damageDealt = rand.nextInt(damage);
                    int damageReceived = rand.nextInt(enemyDamage);
                    enemyHealth -= damageDealt;
                    health -= damageReceived;
                    enemy.setHealth(enemyHealth);
                    p1.setHealth(health);

                    System.out.println("---------------------------------------------------------------------------------");
                    System.out.println("\t You attack " + enemy.getName() + "! You've dealt " + damageDealt + " damage!");
                    System.out.println("\t You receive " + damageReceived + " damage in return from " + enemy.getName() + "!");

                    if(p1.isDead()) {
                        System.out.println("You have taken too much damage! drink a health potion!");
                        break;
                    }

                }
                else if (choice.equals("2")) {
                    if(numHealthPots > 0) {
                        health += healthPotAmount;
                        p1.setHealth(health);
                        numHealthPots--;
                        System.out.println("\t > You use a health potion, healing yourself with " + healthPotAmount + " hitpoints.");
                        System.out.println("\t > You have " + numHealthPots + " potion(s) left.");
                    }
                    else{
                        System.out.println("\t > You got no potions left! Defeat your enemies for a chance to get one!");
                    }

                }
                else if (choice.equals("3")) {
                    if(numAttackPots > 0) {
                        damage += ((attackPotAmount / 4) + rand.nextInt(attackPotAmount));
                        numAttackPots--;

                        enemyHealth -= damage;
                        enemy.setHealth(enemyHealth);

                        System.out.println("\t > You used an attack potion, dealing " + damage + " damage to " + enemy.getName());
                        System.out.println("\t > You have " + numAttackPots + " potion(s) left.");
                        damage -= ((attackPotAmount / 4) + rand.nextInt(attackPotAmount)); // revert damage back to its normal amount
                    }
                    else{
                        System.out.println("\t > You got no potions left! Defeat your enemies for a chance to get one!");
                    }

                }
                else if (choice.equals("4")) {
                    System.out.println("\t You run away from " + enemy.getName() + "!");
                    continue ROUND;
                }
                else{
                    System.out.println("\t Invalid Command!");
                }
            }
            if(p1.isDead()) {
                System.out.println("You narrowly escaped death, crawling with what little health you have left, out of this place.");
                break;
            }
            System.out.println("====================================================================================");
            System.out.println("# " + enemy.getName() + " has been defeated! #");
            System.out.println("# You have " + p1.getHealth() + " HP left! #");

            if(rand.nextInt(100) < healthPotDropRate) {
                numHealthPots++;
                System.out.println("# " + enemy.getName() + " dropped  a health potion! #");
                System.out.println("You now have " + numHealthPots + " health potion(s). #");

            }
            else if(rand.nextInt(100) < attackPotDropRate) {
                numAttackPots++;
                System.out.println("# " + enemy.getName() + " dropped  an attack potion! #");
                System.out.println("You now have " + numAttackPots + " attack potion(s). #");

            }
            System.out.println("====================================================================================");
            System.out.println("What would you like to do now?");
            System.out.println("[1] Continue fighting");
            System.out.println("[2] Exit this place");
            String choice = input.nextLine();

            while(!choice.equals("1") && !choice.equals("2")) {
                System.out.println("Invalid Command!");
                choice = input.nextLine();
            }

            if(choice.equals("1")) {
                System.out.println("You continue on your adventure");
            }
            else if(choice.equals("2")) {
                System.out.println("You exit the place!");
                break;
            }
        }
    }
}