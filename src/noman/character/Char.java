package noman.character;

import noman.weapons.Weapon;
import org.jetbrains.annotations.NotNull;

/**
 * Created by Normie on 8-5-17.
 */

public class Char {

    // The private properties
    private int level;
    private int hitpoints;
    private String name;
    private Weapon weapon;

    // Constructor
    public Char(String name, int level, int hitpoints){
        this.name = name;
        this.level = level;
        // At the beginning the HP en max HP are the same value. Therefor we don't need another argument hitpointMax.
        this.hitpoints = hitpoints;
    }

    public void introduce(){
        System.out.println("\t " + name + " has appeared!");
        System.out.println("\t Level: " + this.getLevel());
    }

    public void info(){
        System.out.println("My name is " + name + ". I am level " + level);
    }
    public int getLevel() {
        return level;
    }

    public void equip(@NotNull Weapon weapon){
        System.out.println("\t" + name + " equipped " + weapon.getName());
        this.weapon = weapon;
    }

    public void attack(@NotNull Char other){
        System.out.println("\t" + name + " attacks " + other.name);
        other.receiveDamage(weapon.getDamage());
    }

    // Handle the damage only if the character is alive.
    public void receiveDamage(int amount){
        if(isDead()){
            System.out.println(name + " already died. Don't be rude and leave the body at peace!");
        }
        else {

            hitpoints -= amount;
            System.out.println(name + "'s hp is reduced by " + amount + " to " + hitpoints);

            if (isDead()) {
                System.out.println(name + " died.");
            }
        }
    }

    // Check if the character is dead
    public boolean isDead(){
        return hitpoints <= 0;
    }

    // Heal the character to full health
    public void heal(){
        System.out.println(name + "'s hit points are fully restored to " + hitpoints);
    }

    public String getName() {
        return name;
    }
    public int getHealth() {
        return hitpoints;
    }

    public int setHealth(int health) {
        return this.hitpoints = health;
    }

}

